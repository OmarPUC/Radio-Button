package com.example.omarsharif.radiobutton;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button showButton;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        showButton = (Button) findViewById(R.id.save);
        textView = (TextView) findViewById(R.id.result);

        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectId = radioGroup.getCheckedRadioButtonId();
                showButton = (RadioButton) findViewById(selectId);

                String value = showButton.getText().toString();
                textView.setText("You have selected: " + value);

                Toast.makeText(MainActivity.this, "Show Selected Data!", Toast.LENGTH_SHORT).show();
            }
        });



    }

    public void radioBtnClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.male:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Male Checked!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.female:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Female Checked!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.other:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Other Checked!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

}
